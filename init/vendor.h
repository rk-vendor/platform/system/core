//
// Created by lili on 18-12-5.
//

#ifndef ANDROID_VENDOR_H
#define ANDROID_VENDOR_H

void vendor_get_block_device_symlinks(struct uevent *uevent, char* device_name, char** links, int* num);
void vendor_init();
#endif //ANDROID_VENDOR_H
